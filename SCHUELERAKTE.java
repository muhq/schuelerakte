public class SCHUELERAKTE
{
    // Spitzname des Schuelers
    public String spitzname;
    // Notenschnitt des Schuelers
    private double notenschnitt;

    public SCHUELERAKTE(double neuerNotenschnitt)
    {
        spitzname = "";
        NotenschnittSetzen(neuerNotenschnitt);
    }

    private void NotenschnittSetzen(double neuerNotenschnitt)
    {
        if (neuerNotenschnitt >= 1.0 && neuerNotenschnitt <= 6.0) {
            notenschnitt = neuerNotenschnitt;
        }
    }
    
    public double NotenschnittGeben() {
        return notenschnitt;
    }
}
