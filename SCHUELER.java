public class SCHUELER
{
    private String name;
    private SCHUELERAKTE akte;

    public SCHUELER(String neuerName, double notenschnitt)
    {
        name = neuerName;
        akte = new SCHUELERAKTE(notenschnitt);
    }
    
    public void ZusammenfassungAusgeben() {
        // AUFGABE 1.
        // TODO: Gibt den Namen des Schuelers, seinen Spitznamen
        //       sowie seinen Notdendurchschnitt aus.
        //
        // Tipp: Um Attribute oder Methoden einer Referenzvariable zu
        //       verwenden benutze die Punktnotation:
        //       <bezeichner>.<attribut>
        //       <bezeichner>.<methode>(<parameter>)
        
        System.out.println("Name: ");
        System.out.println("Spitzname: ");
        System.out.println("Notenschnitt: ");
    }
    
    public void SpitznameSetzen() {
        // AUFGABE 2.
        // TODO: Setze den in der SCHUELERAKTE vermerkten Spitznamen des
        //       Schuelers.
    }
    
    public void NotenschnittSetzen() {
        // AUFGABE 3.
        // TODO: Versuche den Notenschnitt des Schuelers auf 1.0 zu setzen
    }
}
