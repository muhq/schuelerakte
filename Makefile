.PHONY: run all clean

SRC := $(wildcard *.java)
OBJ := $(foreach src, $(SRC), $(subst .java,.class,$(src)))

all: $(OBJ)

clean:
	rm -f *.class

%.class: %.java
	javac $<
